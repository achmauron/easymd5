#include "easymd5.h"
#include "ui_easymd5.h"
#include <QFileDialog>
#include <QCryptographicHash>
#include <QMessageBox>

QString FilePath,Hash;

EasyMD5::EasyMD5(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::EasyMD5)
{
    ui->setupUi(this);
}

EasyMD5::~EasyMD5()
{
    delete ui;
}

void EasyMD5::formLoad(int argc, char *argv[])
{
    if(argc > 1)
    {
        FilePath = QString(argv[1]);
        ui->lineEdit->setText(FilePath);
        callCal();
    }
}

void EasyMD5::on_pushButton_clicked()
{
    FilePath = QFileDialog::getOpenFileName(this,tr("Open file"),"",tr("All files (*)"));
    ui->lineEdit->setText(FilePath);
}

void EasyMD5::on_pushButton_2_clicked()
{
    FilePath = ui->lineEdit->text();
    callCal();
}

void EasyMD5::on_pushButton_3_clicked()
{
    if(!ui->lineEdit_3->text().compare(Hash,Qt::CaseInsensitive))
        QMessageBox::information(this,tr("EasyMD5"),tr("Hash is correct."),QMessageBox::Ok);
    else
        QMessageBox::critical(this,tr("EasyMD5"),tr("Hash is incorrect."),QMessageBox::Ok);
}

void EasyMD5::setProgressText(bool e)
{
    ui->progressBar->setTextVisible(e);
}

void EasyMD5::callCal()
{
    ui->lineEdit->setDisabled(true);
    ui->pushButton->setDisabled(true);
    ui->pushButton_2->setDisabled(true);
    ui->pushButton_3->setDisabled(true);
    QMessageBox *mError = new QMessageBox;
    hashCal *oCal = new hashCal;
    mError->setText(tr("Error reading from file."));
    mError->setWindowTitle(tr("EasyMD5"));
    mError->setIcon(QMessageBox::Critical);
    connect(oCal,SIGNAL(progress(int)),ui->progressBar,SLOT(setValue(int)));
    connect(oCal,SIGNAL(error()),mError,SLOT(exec()));
    connect(oCal,SIGNAL(finished(QString)),ui->lineEdit_2,SLOT(setText(QString)));
    connect(oCal,SIGNAL(enableUI(bool)),ui->lineEdit,SLOT(setEnabled(bool)));
    connect(oCal,SIGNAL(enableUI(bool)),ui->pushButton,SLOT(setEnabled(bool)));
    connect(oCal,SIGNAL(enableUI(bool)),ui->pushButton_2,SLOT(setEnabled(bool)));
    connect(oCal,SIGNAL(enableUI(bool)),ui->pushButton_3,SLOT(setEnabled(bool)));
    connect(oCal,SIGNAL(setProgressText(bool)),this,SLOT(setProgressText(bool)));
    oCal->start();
}

void hashCal::run()
{
    QCryptographicHash hash(QCryptographicHash::Md5);
    qint64 FileSize;
    qint64 Complate = 0;
    QFile file(FilePath);
    const int BuffSize = 65535;
    char buff[BuffSize];
    int ReadLen = 0;
    emit setProgressText(true);
    emit progress(0);

    if(file.open(QIODevice::ReadOnly))
    {
        FileSize = file.size();
        while((ReadLen = file.read(buff,BuffSize)) > 0)
        {
            Complate += ReadLen;
            hash.addData(buff,ReadLen);
            emit progress(100*Complate/FileSize);
        }
        file.close();
        Hash = QString(hash.result().toHex());
        emit finished(Hash);
        emit progress(100);
        emit enableUI(true);
    }
    else
    {
        emit error();
        emit progress(0);
        emit setProgressText(false);
        emit enableUI(true);
    }
}
