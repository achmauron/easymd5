#ifndef EASYMD5_H
#define EASYMD5_H

#include <QMainWindow>
#include <QThread>

namespace Ui {
class EasyMD5;
}

class EasyMD5 : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit EasyMD5(QWidget *parent = 0);
    void formLoad(int argc, char *argv[]);
    void callCal();
    ~EasyMD5();
    
private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

public slots:
    void setProgressText(bool);

private:
    Ui::EasyMD5 *ui;
};

class hashCal : public QThread
{
    Q_OBJECT
protected:
    void run();
signals:
    void progress(int);
    void error();
    void finished(QString);
    void enableUI(bool);
    void setProgressText(bool);
};

#endif // EASYMD5_H
