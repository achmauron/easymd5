#-------------------------------------------------
#
# Project created by QtCreator 2012-07-14T13:56:58
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = easymd5
TEMPLATE = app


SOURCES += main.cpp\
        easymd5.cpp

HEADERS  += easymd5.h

FORMS    += easymd5.ui
